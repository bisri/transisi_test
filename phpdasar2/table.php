<!DOCTYPE html>
<html>
<body>
   
<?php
    function table()
    {
        $number = 64;
        $row_and_cell = sqrt($number);
        $x = 1;
        echo '<table>';

        for ($i=1; $i <= $row_and_cell; $i++) {
            echo '<tr>';
            for($n=1; $n <= $row_and_cell; $n++){
                if($x % 3 == 0 || $x % 4 == 0){
                    echo '<td>' . $x++ . '</td>';
                }else{
                    echo '<td style="background-color: black; color: white;">' . $x++ . '</td>';
                }
            }
            echo '</tr>';
        }

        echo '</table>';
    }

    table();
?>

</body>
</html>