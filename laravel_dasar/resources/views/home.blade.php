@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-2">
        <div class="border-end bg-white container" id="sidebar-wrapper">
            <h3>Transisi</h3>
            <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="{{route('company')}}">Company</a>
                <a class="list-group-item list-group-item-action list-group-item-light p-3" href="{{route('employee')}}">Employee</a>
            </div>
        </div>
    </div>
    <div class="col-md-10">
        <div class="container">
            @yield('main-content')
        </div>
    </div>
</div>
@endsection
