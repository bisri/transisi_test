@extends('home')

@section('main-content')

<form action="{{route('company.update', $result->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="modal-title-default">Edit Company</h6>
        </div>
        <div class="modal-body">
        <div class="row">
            <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <label class="form-control-label" for="input-name">Nama</label>
                <input type="text" class="form-control" id="input-name" name="nama" value="{{ $result->nama }}" placeholder="Nama">
            </div>
            </div>
            <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <label class="form-control-label" for="input-email">Email</label>
                <input type="email" class="form-control" id="input-email" name="email" value="{{ $result->email }}" placeholder="Email">
            </div>
            </div>
            <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <label class="form-control-label" for="input-logo">Logo</label>
                <input type="file" class="form-control" id="input-logo" name="logo">
            </div>
            </div>
            <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <label class="form-control-label" for="input-web">Website</label>
                <input type="text" class="form-control" id="input-web" name="website" value="{{ $result->website }}" placeholder="Website">
            </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{route('company')}}" class="btn btn-link  ml-auto" data-dismiss="modal">Back</a>
        </div>
    </div>
</form>
@endsection