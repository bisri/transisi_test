@extends('home')

@section('main-content')
<div class="card">
    <div class="card-header">
        <h6>Employee</h6>
        
    </div>
    <div class="col" style="margin-top: 5px;">
        <a class="btn btn-primary" href="{{route('employee.create')}}">Create</a>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
          Import
        </button>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="{{route('employee.import')}}" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
                @csrf
                @method('POST')
                <div class="row">
                  <div class="col">
                    <input type="file" class="form-control" name="excel">
                  </div>
                </div>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Import</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="card-body">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama</th>
                <th scope="col">Company</th>
                <th scope="col">Email</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                @foreach ($results as $result)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$result->nama}}</td>
                        <td>{{$result->company->nama}}</td>
                        <td>{{$result->email}}</td>
                        <td>
                          <form action="{{ route('employee.destroy', $result->id) }}" method="post" id="form-{{ $result->id }}">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-success btn-sm text-white" href="{{route('employee.edit', $result->id)}}">
                              Edit
                            </a>
                            <button type="submit" class="btn btn-danger btn-sm text-white btn-hapus" data-id="{{ $result->id }}">
                              Delete
                            </button>
                          </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-md-6 col-sm-12 text-sm">
          Showing {{ $results->firstItem() }} to {{ $results->lastItem() }} of {{ $results->total() }} result
        </div>
        <div class="col-md-6 col-sm-12">
          <div class="float-right">
            {{ $results->links() }}
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

{{-- @section('custom-js')
<script>
  $(document).ready(()=>{
    $('.btn-hapus').on('click', function (e) {
      console.log('haha')
      let id = e.target.dataset['id']

      Swal.fire({
        title: 'Warning',
        text: 'Are you sure?',
        icon: 'warning',
        showCancelButton: true,
      }).then((data) => {
        if (data.isConfirmed) {
          $(`#form-${id}`).submit()
        }
      })
    })
  })
  
</script>
@endsection --}}