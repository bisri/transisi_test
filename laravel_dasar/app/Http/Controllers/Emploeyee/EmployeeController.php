<?php

namespace App\Http\Controllers\Emploeyee;

use App\Http\Controllers\Controller;
use App\Http\Requests\Employee\StoreRequest;
use App\Http\Requests\Employee\UpdateRequest;
use App\Imports\EmployeesImport;
use App\Services\CompanyService;
use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function index(Request $request)
    {
        try {
            $results = $this->employeeService->getAll($request);

            return view('dashboard.employee.index', compact('results'));

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
        
    }

    public function getCompany(Request $request, CompanyService $companyService)
    {
        try {
            $results = $companyService->getAll($request);

            return response()->json($results);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            
            return view('dashboard.employee.create');

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        try {
            $this->employeeService->store($request);

            return redirect()->route('employee')->with('Success', 'Data Ditambahkan');

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    public function import(Request $request)
    {
        try {

            // Excel::load($request->file('excel'))->chunk(10, function($results)
            // {
            //         foreach($results as $row)
            //         {
            //             dd($row);
            //         }
            // });
            
            Excel::import(new EmployeesImport, $request->file('excel'), 's3', \Maatwebsite\Excel\Excel::XLSX);

            return redirect()->route('employee')->with('success', 'Data Imported');

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            
            $result = $this->employeeService->getOne($id);

            return view('dashboard.employee.edit', compact('result'));

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        try {
            $this->employeeService->update($request, $this->employeeService->getOne($id));

            return redirect()->route('employee')->with('success', 'Data dibuat');
        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            
            $this->employeeService->destroy($this->employeeService->getOne($id));

            return redirect()->route('employee')->with('success', 'Data dihapus');

        } catch (\Throwable $th) {
            Log::error("Controller error. " . $th->getMessage());
    		abort(500);
        }
    }
}
