<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

function makeHash($string)
{
	try {
		return Hash::make($string);
	} catch (\Throwable $th) {
		Log::error("Helper error. " . $th->getMessage());
		abort(500);
	}
}

function uploadFile($file, $path, $isBase64 = false)
{
	try {
		if ($isBase64) {
			$path = str_replace('_', '-', $path) . '/';
		    $file = base64_decode(str_replace('base64,', '', explode(';', $file)[1]), true);
		    $name = substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 15) . '.webp';

		    Storage::disk(env('STORAGE', 'local'))->put('/' . $path . $name, $file);

	    	return 'storage/' . $path . $name;
		} else {
			$path    = str_replace('_', '-', $path);
			$ext     = $file->getClientOriginalExtension();
			$newFile = Storage::disk(env('STORAGE', 'local'))->put('public/' . $path, $file);
			$arr     = explode('/', $newFile);

	    	return 'storage/' . $path . '/' . end($arr);
		}
	} catch (\Throwable $th) {
	    Log::error("Helper error. " . $th->getMessage());
	    abort(500);
	}
}

function deleteFile($path)
{
	try {
    	return unlink(public_path($path));
	} catch (\Throwable $th) {
	    Log::error("Helper error. " . $th->getMessage());
	    abort(500);
	}
}

function makeSlug($string)
{
	try {
    	return strtolower(str_replace(' ', '-', $string));
	} catch (\Throwable $th) {
	    Log::error("Helper error. " . $th->getMessage());
	    abort(500);
	}
}

function generateTrxCode()
{
	try {
    	return 'TRX-' . date('YmdHis');
	} catch (\Throwable $th) {
	    Log::error("Helper error. " . $th->getMessage());
	    abort(500);
	}
}
