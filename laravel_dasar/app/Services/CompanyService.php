<?php

namespace App\Services;

use App\Models\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CompanyService 
{
    public function getOne($id)
    {
        try {
            
            $result = Company::with('employees')->findOrFail($id);

            return $result;

        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try {
            if ($request->ajax()) {
                
                $term = trim($request->term);
                $companies = Company::when($term, function ($query) use ($request) {
                            $query->where('nama', 'like', '%' . trim($request->term) . '%');
                        })
                        ->orderBy('id', 'desc')
                        ->paginate(5);
               
                $morePages=true;
                $pagination_obj= json_encode($companies);
                if (empty($companies->nextPageUrl())){
                    $morePages=false;
                }
                $results = array(
                  "results" => $companies->getCollection()->transform(function($company){
                      return $this->select2Format($company);
                  }),
                  "pagination" => array(
                    "more" => $morePages
                  )
                );
            
                return $results;
            }else{
                $results = Company::when($request->text_search, function ($query) use ($request) {
                        $query->where('nama', 'like', '%' . $request->text_search . '%');
                    })
                    ->orderBy('id', 'desc')
                    ->paginate(5);
        
                return $results;
            }

        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            $path = '';

            if ($request->file('logo')) {
                $path = uploadFile($request->file('logo'), 'company');
            }
            DB::beginTransaction();

            $result = Company::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'logo' => $path,
                'website' => $request->website,
            ]);

            DB::commit();

            return $result;
        }catch(\Throwable $th){
            DB::rollBack();
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function update($request, Company $company)
    {
        try{
            $path = $company->logo;

            if ($request->file('logo')) {
                $path = uploadFile($request->file('logo'), 'company');
            }

            $result = $company->update([
                'nama' => $request->nama,
                'email' => $request->email,
                'logo' => $path,
                'website' => $request->website,
            ]);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error ' . $th->getMessage());
            abort(500);
        }
    }

    public function destroy(Company $company)
    {
        try {
            deleteFile($company->logo);
            
 			$result = $company->delete();

	   		return $result;

 		} catch (\Throwable $th) {
 			Log::error("Service error. " . $th->getMessage());
 			abort(500);
 		}
    }

    public function select2Format($company)
    {
        return [
            'id' => $company->id,
            'text' => $company->nama
        ];
    }
}