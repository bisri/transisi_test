<?php
    
    function cari($string){
        $str = str_split($string);
        
        $arr = [
            ['f', 'g', 'h', 'i'],
            ['j', 'k', 'p', 'q'],
            ['r', 's', 't', 'u']
        ];

        //apakah stringnya ada di array?
        for ($i=0; $i < count($str); $i++) { 
            for ($n=0; $n < count($arr); $n++) { 
                if(!in_array($str[$i], $arr[$n])){
                    if($arr[$n] == end($arr)){
                        return 0;
                        die();
                    }
                }else{
                    break;
                }
            }
        }

        //jika ada huruf dobel
        if($str != array_unique($str))
        {
            return 0;
            die();
        }

        //apakah data berurutan seperti 'fghi'
        for ($i=0; $i < count($arr); $i++) { 
            if($str == $arr[$i])
            {
                return 1;
                die();
            }
        }

        //jika ada identifikasi indexnya dan push ke array untuk statement kondisi
        $arrayMatch = [];
        for ($i=0; $i < count($str); $i++) { 
            for ($n=0; $n < count($arr); $n++) { 
                if(array_search($str[$i], $arr[$n]) > -1)

                array_push($arrayMatch, [$n, array_search($str[$i], $arr[$n])]);
            }
        }

        /**
         * algoritma yang ditangkap :
         * index pertama tidak boleh urut seperti [0][0], [1][1], [2][2]
         * index kedua sebelum terakhir harus sama dengan index sebelumnya
         * index kedua urut dan index pertama sama
         * jika index kedua tidak urut maka index pertama harus beda dan dan index kedua harus sama dengan sebelumnya
         */

         //index pertama tidak boleh urut dan index kedua harus sama
         //index pertama urut/beda maka index kedua harus sama dari index kedua sebelumnya
         
        
         for ($i=0; $i < count($arrayMatch); $i++) { 

             $cur = current($arrayMatch[$i]);
             
             if($i < count($arrayMatch) && $i != 0){
                
                 $op = current($arrayMatch[$i-1]);
                 
                 if($cur != $op){
                    $opEnd = end($arrayMatch[$i-1]);
                    $curEnd = end($arrayMatch[$i]);
                    
                    if($cur != $op && $opEnd != $curEnd){
                        return 0;
                        break;
                    }else{
                        return 1;
                        break;
                    }

                 }
                 
             }
             
         }

    }

    echo 'true = 1 <br>';
    echo 'false = 2 <br><br>';

    echo "cari fghi = " . cari('fghi') . '<br>';
    echo "cari fghp = " . cari('fghp') . '<br>';
    echo "cari fjrstp = " . cari('fjrstp') . '<br>';
    echo "cari fghq = " . cari('fghq') . '<br>';
    echo "cari fst = " . cari('fst') . '<br>';
    echo "cari pqr = " . cari('pqr') . '<br>';
    echo "cari fghh = " . cari('fghh') . '<br>';
       
?>
