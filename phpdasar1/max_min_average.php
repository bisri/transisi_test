<!DOCTYPE html>
<html>
<body>

<?php
    $str = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";

    //to array
    $arr = explode(" ", $str);

    /**
     * Rata-rata = jmlh_seluruh/total_bil
     * =====================================
     */
        $average = array_sum($arr)/count($arr);
        echo '<h3>Rata-rata = ' . $average . '</h3>';

    /**
     * Get 7 nilai tertinggi && terendah
     * filter duplicate data
     * =====================================
     */
        //filter duplikat dan reindexing
        $arr = array_values(array_unique($arr));

        /**
         * 7 Nilai tertinggi
         * sort desc
         */
        rsort($arr);
        $nilaiTertinggi = array_slice($arr, 0, 7);
        echo '<h3>7 nilai tertinggi = ' . implode(", ", $nilaiTertinggi) . '</h3>';

        /**
         * 7 Nilai terendah
         * sort asc
         */
        sort($arr);
        $nilaiTerendah = array_slice($arr, 0, 7);
        echo '<h3>7 nilai terendah = ' . implode(", ", $nilaiTerendah) . '</h3>';
        
?> 

</body>
</html>