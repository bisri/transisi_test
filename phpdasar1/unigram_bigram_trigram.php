<!DOCTYPE html>
<html>
<body>

<form action="" method="POST">
    <input type="text" name="str" value="Jakarta adalah ibukota negara Republik Indonesia">
    <button type="submit">Submit</button>
</form>
<div>
    
<?php
    function createUBT($str)
    {
        $str = strtolower($str);
        $arrStr = explode(' ', $str);

		// unigram
		$unigram = '';
		foreach ($arrStr as $item) {
			$unigram .= $item.', ';
		}
        
		$unigram = substr($unigram, 0, -2);

		// bigram
		$x = 0;
		$bigram = '';
		foreach ($arrStr as $item) {
			if ($x < 1) {
				$bigram .= $item.' ';
				$x++;
			} else {
				$bigram .= $item.', ';
				$x = 0;
			}
		}
		$bigram = substr($bigram, 0, -2);

		// trigram
		$y = 0;
		$trigram = '';
		foreach ($arrStr as $item) {
			if ($y < 2) {
				$trigram .= $item.' ';
				$y++;
			} else {
				$trigram .= $item.', ';
				$y = 0;
			}
		}
		$trigram = substr($trigram, 0, -2);


		$result = 'Unigram : '. $unigram . '<br>';
		$result .= 'Bigram : '. $bigram . '<br>';
		$result .= 'Trigram : '. $trigram;

		return $result;
    }

    if(!empty($_POST['str'])){
        echo createUBT($_POST['str']);
    }
?>
</div>

</body>
</html>